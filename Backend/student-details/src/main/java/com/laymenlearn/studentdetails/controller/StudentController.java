package com.laymenlearn.studentdetails.controller;

import com.laymenlearn.studentdetails.model.dto.StudentDto;
import com.laymenlearn.studentdetails.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
//SLF4J is log provider
@Slf4j
//Here rest controller base path is student
@RestController("/student")
public class StudentController {

    @Autowired
    private StudentService service;

    //Get student list
    @GetMapping("/")
    public ResponseEntity<List<StudentDto>> getStudents(){
        return new ResponseEntity<List<StudentDto>>(service.getStudents(), HttpStatus.OK);
    }

    //Add student to the list
    @PostMapping("/")
    public ResponseEntity<StudentDto> createStudent(@RequestBody StudentDto requestData){
        return new ResponseEntity<StudentDto>(service.createStudent(requestData), HttpStatus.CREATED);
    }
}
