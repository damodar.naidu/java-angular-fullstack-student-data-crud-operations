package com.laymenlearn.studentdetails.model.dto;

import com.laymenlearn.studentdetails.model.entity.Student;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class StudentDto {
    private Long sno;
    private String name;
    private String city;
    private String course;
    private int semester;

    public Student toEntity(){
        Student entity = new Student();
        BeanUtils.copyProperties(this, entity);
        return entity;
    }

    public static StudentDto toDto(Student student){
        StudentDto dto = new StudentDto();
        BeanUtils.copyProperties(student, dto);
        return dto;
    }
}
