package com.laymenlearn.studentdetails.repository;

import com.laymenlearn.studentdetails.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}
