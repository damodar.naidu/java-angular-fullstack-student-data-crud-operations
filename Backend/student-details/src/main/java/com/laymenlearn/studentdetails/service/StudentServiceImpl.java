package com.laymenlearn.studentdetails.service;

import com.laymenlearn.studentdetails.model.dto.StudentDto;
import com.laymenlearn.studentdetails.model.entity.Student;
import com.laymenlearn.studentdetails.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class StudentServiceImpl  implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public StudentDto createStudent(StudentDto studentDto) {
        //TODO put validations
        Student student = studentDto.toEntity();
        student = studentRepository.save(student);
        return StudentDto.toDto(student);
    }

    @Override
    public List<StudentDto> getStudents() {
        //TODO can be added pagination
        List<Student> students = studentRepository.findAll();
        return students.stream().map(StudentDto::toDto).collect(Collectors.toList());
    }
}
