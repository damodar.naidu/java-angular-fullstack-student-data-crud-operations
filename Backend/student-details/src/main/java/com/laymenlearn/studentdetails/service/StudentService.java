package com.laymenlearn.studentdetails.service;


import com.laymenlearn.studentdetails.model.dto.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto createStudent(StudentDto studentDto);
    List<StudentDto> getStudents();
}
